FROM alpine:3.14
RUN apk add --no-cache bc
COPY ./entrypoint.sh ./events ./app-* /
ENTRYPOINT ["/entrypoint.sh"]
